# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 17:31:58 2020

@author: Nicolas Cadieux
GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify it under
the terms of theGNU General Public License as published by the Free Software
Foundation, either version 3 of theLicense, or (at your option) any later
version.This program is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.You should have received a copy of the GNU
General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.

NEW FEATURES, FIXES AND KNOW ISSUES
*******New: v0_2Beta All
*******Fixes: 
*******Know issues:
    Must use Output from version 1.1 of
    upstream_downstream_shortests_path_dijkstra.py as output format has been
    modified.  Contact author if you want a version compatible with version 1 
    outputs.
    

*******TODO:
    -Trouble shoot when no first data line found in Check Header
    -Change input for old file format with route field
    -Better alpha numerica sorting for merging files
"""

import os
import sys
from multiprocessing import Pool
import multiprocessing
import time


START_TIME = time.time()

# ****************************** USER VARIABLES****************
# SECTION 1: Make the bottom matix for the output of 
# upstream_downstream_shortests_path_dijkstra_v1_1_.py
# Input path for results of upstream_downstream_shortests_path_dijkstra_v1_1_.py
CREATE_BOTTOM_MATRIX = True
WRITE_OUTPUT_HEADER = True # ignored if CREATE_BOTTOM_MATRIX = False
CREATE_DIAGONAL_FILE = True
DIAGONAL_VALUE = 0 # All distances in diagonal will be set to DIAGONAL_VALUE
UPDOWN_DIJKSTRA_INPUT_PATH = r'C:\temp\Sample_data_sample_basin\results from Upstream Downstream Shortest Path v 1_1\Routes'
DIAGONAL_FILE_NAME = r'routes_sample_diagonal.csv'
#**************************************************************
#SECTION 2: Merge all routes into one files
MERGE_FILES = True 
WRITE_OUTPUT_HEADER_MERGE_FILE = True # ignored if MERGE_FILE = False
MERGED_FILE_NAME = r'Sample_routes_all.csv'# use .csv

THREADS = multiprocessing.cpu_count()
# ************************** OUTPUT DIRECTORIES *****************************
# Default: if UPDOWN_DIJKSTRA_INPUT_PATH = c:\temp
# c:\temp\Input_bassin_files.csv
# c:\temp\Output_bassin_lower_matrix_files.csv
# c:\temp\Diagonal\Output_Diagonal_file.csv
# c:\temp\Merged_file\Output_Merged_file.csv

BOTTOM_HALF_MATRIX_OUTPUT_PATH = os.path.join(UPDOWN_DIJKSTRA_INPUT_PATH, '') # ex: r'C:\Temp\Results_sample_basin\Routes\matrices'
DIAGONAL_FILE_INPUT_PATH = BOTTOM_HALF_MATRIX_OUTPUT_PATH #use ouptup of Section 1
DIAGONAL_FILE_OUTPUT_PATH = os.path.join(UPDOWN_DIJKSTRA_INPUT_PATH, 'Diagonal')
MERGE_FILE_INPUT_PATH = BOTTOM_HALF_MATRIX_OUTPUT_PATH #use ouptup of Section 1
MERGE_FILE_OUTPUT_PATH = os.path.join(BOTTOM_HALF_MATRIX_OUTPUT_PATH,'Merged_file')#ex: r'C:\Temp\Results_sample_basin\Routes\Merged_files'
GEODA_FILE_INPUT_PATH = BOTTOM_HALF_MATRIX_OUTPUT_PATH #use ouptup of Section 1
# ****************************** USER VARIABLES END****************


# *********************   GLOBAL VARIABLES    *********************************
# Change Global variables at your own risk.  Backup the original python script
#POINT_SHP_FILE_FULL_PATH_2_GEODA_HEADER = os.path.splitext(r'CommunityPoints_updated_epsg32718.shp')[0]# use this if you don't have acces to the shape file. 
#POINT_SHP_FILE_FULL_PATH_2_GEODA_HEADER = os.path.splitext(os.path.basename(POINT_SHP_FILE_FULL_PATH))[0] #TODO CHANGE USER GUIDE  
WEIGHT_POSITION = 9 # should be 9 unless you change the file format.
INPUT_FILE_SEP = ';' # should be ';' unless you change the file format.
OUTPUT_FILE_SEP = ';' #TODO KNOW ERROR CHANGING TO ',' WILL GIVE PROBLEMS WITH WKT ROUTE.
#LINE_BUFFER = 1 # Line_Buffer in m or degrees Used to convert line to polygons for the N_component layer. #TODO explain in user guide
                

def create_directory(path: str):
    """
  
    Parameters
    ----------
    path : str
        DESCRIPTION.

    Returns
    -------
    err : TYPE
        DESCRIPTION.

    """
    
    """Create a directory. Will pass if directory has been added
      by another tread."""

    if os.path.isdir(path):
        pass
    else:
        try:
            os.makedirs(path)
        except WindowsError as err:
            pass
            return err
        
def scan_directory(path: str,extension_filter = '',filename_filter = '',
                   search_subdirectory = False):
    """
        
    Scan directory and subdirectories (if search_subdirectory = True).Filters file
    by extension and file name.

    Parameters
    ----------
    path : str
        DESCRIPTION.
    extension_filter : TYPE, optional
        DESCRIPTION. The default is ''.
    filename_filter : TYPE, optional
        DESCRIPTION. The default is ''.
    search_subdirectory : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    file_lst : TYPE
        DESCRIPTION.

    """
    file_lst = []
    for (directory, sub_directory, file_name) in os.walk(path):
        for files in file_name:
            if files.lower().endswith(extension_filter) and filename_filter.lower() in files.lower():# and files.lower().startswith("variable")
                file_filter = os.path.join(directory, files)
                file_lst.append(file_filter)
        if search_subdirectory == False:
            break #this break will exit the loop and limite to 1 directory only
        elif search_subdirectory == True:
            pass
            
    return file_lst

def check_header(file_name:str):
    """
    

    Parameters
    ----------
    file_name : str
        DESCRIPTION.

    Returns
    -------
    n_skipline : TYPE
        DESCRIPTION.
    n_data_fields : TYPE
        DESCRIPTION.
    source_target_str : TYPE
        DESCRIPTION.
    target_source_str : TYPE
        DESCRIPTION.
    header_lines : TYPE
        DESCRIPTION.

    Normal input order should be:
        inputline_id = 0 #This line will not be in the output
        source = 1
        target = 2
        source_xy = 3
        target_xy = 4
        downstream_pct = 5
        upstream_pct = 6
        downstream_length = 7
        upstream_length = 8
        total_length = 9
        wkt = 10 (optional)
        
    """
    input_file = open(file_name,'r')    
    n_skipline = 0
    header_lines = ''
    for lines in input_file:
        try:
            int(lines.split(INPUT_FILE_SEP)[0])
            first_data_line = input_file.readline()
            n_data_fields = len(first_data_line.split(INPUT_FILE_SEP))
            if n_data_fields == 11:
                source_target_str = (0,1,2,3,4,5,6,7,8,9,10)
                target_source_str = (0,2,1,4,3,6,5,8,7,9,10)
                break # break out of loop if int is found
            
            elif n_data_fields == 10:
                source_target_str = (0,1,2,3,4,5,6,7,8,9)
                target_source_str = (0,2,1,4,3,6,5,8,7,9)
                break # break out of loop if int is found
            else:
                pass
                #TODO: ERROR HANDLING if can't find header or can't find data.
                # Curent error is UnboundLocalError: local variable \
                #'n_data_fields' referenced before assignment
                sys.exit(
                    'Check your input data, Check_Header function\
                    cannot parse your data fields Too many field???\
                        Wrong csv file separator???')
                                
        except ValueError:
            header_lines = header_lines + lines
            n_skipline += 1
            #TODO if first data line is not found, return error (ex empty file)
            
    input_file.close()
    return n_skipline, n_data_fields, source_target_str,target_source_str, header_lines
    
def make_bottom_matrix(files_path:str):
    """
    

    Parameters
    ----------
    files_path : str
        DESCRIPTION.

    Returns
    -------
    None.

    """
    in_file_name = os.path.basename(files_path)
    out_file_name = os.path.splitext(in_file_name)[0] + '_bottom_matrix.csv'
    output_file_path_out_file_name = os.path.join(BOTTOM_HALF_MATRIX_OUTPUT_PATH, out_file_name)
    create_directory(BOTTOM_HALF_MATRIX_OUTPUT_PATH)
    header = (check_header(files_path))
    n_skipline = int(header[0])
    n_data_fields = header[1]
    source_target_str = header[2]
    target_source_str = header [3]
    header_lines = header [4]
    input_file = open(files_path,'r')
    output_file = open(output_file_path_out_file_name, 'w')
    data = []

    #write header in output file
    if WRITE_OUTPUT_HEADER == True:
        output_file.write(header_lines)

    #advance header
    for w in range(0,n_skipline):
        input_file.readline()
        
    for lines in input_file:
        # data_line = ''    
        # for w in source_target_str:
        #     data_line = data_line+(lines.split(INPUT_FILE_SEP)[w])
        #     if w < n_data_fields -1:
        #         data_line = data_line+OUTPUT_FILE_SEP
        # data.append(data_line)
        data_line = ''    
        for w in target_source_str:
            data_line = data_line+(lines.split(INPUT_FILE_SEP)[w])
            if w < n_data_fields -1:
                data_line = data_line+OUTPUT_FILE_SEP
        data.append(data_line)
    
    for lines in data:
        output_file.write(lines)
    #Close all files
    input_file.close()
    output_file.close()
    return None
    
def make_diagonal(files_path: list,value_int = DIAGONAL_VALUE):
    """
    

    Parameters
    ----------
    files_path : list
        DESCRIPTION.
    value_int : TYPE, optional
        DESCRIPTION. The default is DIAGONAL_VALUE.

    Returns
    -------
    None.
    Normal input order should be:
        inputline_id = 0 #This line will not be in the output
        source = 1
        target = 2
        source_xy = 3
        target_xy = 4
        downstream_pct = 5
        upstream_pct = 6
        downstream_length = 7
        upstream_length = 8
        total_length = 9
        wkt = 10 (optional)
    """
    #Take header from first file
    header = check_header(files_path[0])
    header_lines = header[4]
    n_data_fields=len(header[2])
    create_directory(DIAGONAL_FILE_OUTPUT_PATH)
    output_file_path_out_file_name = os.path.join(DIAGONAL_FILE_OUTPUT_PATH,
                                                  DIAGONAL_FILE_NAME)
    output_file = open(output_file_path_out_file_name, 'w')

    inputline_id = '-9999' #This line will not be in the output
    downstream_pct = str(DIAGONAL_VALUE)
    upstream_pct = str(DIAGONAL_VALUE)
    downstream_length = str(DIAGONAL_VALUE)
    upstream_length = str(DIAGONAL_VALUE)
    total_length = str(DIAGONAL_VALUE)
    
    # Write header
    if WRITE_OUTPUT_HEADER == True:
        output_file.write(header_lines)
    data_dct = {}
    for f in files_path:
        header = (check_header(f))
        n_skipline = header[0]
        input_file = open(f,'r')
    # Skip over header on file
        for w in range(0,n_skipline):
            input_file.readline()
            
        data_line = ''
        for line in input_file:
            unique_id = str(line.split(INPUT_FILE_SEP)[1])
            if unique_id in data_dct:
                pass
            else:
                if n_data_fields == 11:
                    source = str(line.split(INPUT_FILE_SEP)[1])
                    target = source
                    source_xy = str(line.split(INPUT_FILE_SEP)[3])
                    target_xy = source_xy
                    wkt = source_xy
                    #source_target_str = (0,1,2,3,4,5,6,7,8,9,10)
                    data_line = inputline_id+OUTPUT_FILE_SEP\
                            +source+OUTPUT_FILE_SEP\
                            +target+OUTPUT_FILE_SEP\
                            +source_xy+OUTPUT_FILE_SEP\
                            +target_xy+OUTPUT_FILE_SEP\
                            +downstream_pct+OUTPUT_FILE_SEP\
                            +upstream_pct+OUTPUT_FILE_SEP\
                            +downstream_length+OUTPUT_FILE_SEP\
                            +upstream_length+OUTPUT_FILE_SEP\
                            +total_length+OUTPUT_FILE_SEP\
                            +wkt+'\n'

                elif n_data_fields == 10:
                    source = str(line.split(INPUT_FILE_SEP)[1])
                    target = source
                    source_xy = str(line.split(INPUT_FILE_SEP)[3])
                    target_xy = source_xy
                    data_line = inputline_id+OUTPUT_FILE_SEP\
                            +source+OUTPUT_FILE_SEP\
                            +target+OUTPUT_FILE_SEP\
                            +source_xy+OUTPUT_FILE_SEP\
                            +target_xy+OUTPUT_FILE_SEP\
                            +downstream_pct+OUTPUT_FILE_SEP\
                            +upstream_pct+OUTPUT_FILE_SEP\
                            +downstream_length+OUTPUT_FILE_SEP\
                            +upstream_length+OUTPUT_FILE_SEP\
                            +total_length+OUTPUT_FILE_SEP+'\n'
                            
                data_dct[unique_id]=data_line
        input_file.close()
        
    for k in data_dct:
        output_file.write(data_dct[k])
    output_file.close()

    return None

def merge_files(files_path: list):
    """
    

    Parameters
    ----------
    files_path : list
        DESCRIPTION.

    Returns
    -------
    None.

    
    Stitch all output files together.
    Writes one header if WRITE_OUTPUT_HEADER_MERGE_FILE = True
    """
    #Take header from first file
    header = check_header(files_path[0])
    header_lines = header[4]
    create_directory(MERGE_FILE_OUTPUT_PATH)
    output_file_path_out_file_name = os.path.join(
        MERGE_FILE_OUTPUT_PATH,MERGED_FILE_NAME
        )
    output_file = open(output_file_path_out_file_name, 'w')
    
    # Write header
    if WRITE_OUTPUT_HEADER == True:
        output_file.write(header_lines)

    for f in files_path:
        data = []
        header = (check_header(f))
        n_skipline = header[0]
        input_file = open(f,'r')
    # Skip over header on file
        for w in range(0,n_skipline):
            input_file.readline()        
        
        for line in input_file:
            data.append(line)
        
        for lines in data:
            output_file.write(lines)
        input_file.close()
    output_file.close()
    return None


if __name__ == '__main__':
    

    if CREATE_BOTTOM_MATRIX == True:
        p = Pool(THREADS)
        files = (scan_directory(UPDOWN_DIJKSTRA_INPUT_PATH,'.csv'))
        
        #Fast check for data format
        print('Checking file formats')
        for f in files:
            check_header(f)
        print('Checking file formats: Done')

        for n, f in enumerate(files):
            if 'bottom_matrix' in f:
                sys.exit(
                    'Delete your bottom_matrix file from the input directory')
        p.map(make_bottom_matrix,files)
        p.close()
        p.join()
        print ('CREATE_BOTTOM_MATRIX: done','\n')

    if CREATE_DIAGONAL_FILE == True:
        if CREATE_BOTTOM_MATRIX == False:
            err = input(
                'CREATE_BOTTOM_MATRIX is set to False. '\
                    +'Diagonal file will be incomplete if'\
                    +' bottom_matrix is not already in '\
                    +' your input directory. Press any key to continue')
                
            print(err)
        files = (scan_directory(DIAGONAL_FILE_INPUT_PATH,'.csv'))
        for f in files:
            if DIAGONAL_FILE_NAME in f:
                sys.exit ('Delete', DIAGONAL_FILE_NAME,
                          'from your input directory.')
        res_write_diag = make_diagonal(files, DIAGONAL_VALUE)
        print ('CREATE_DIAGONAL_FILE: done','\n')
    
    if MERGE_FILES == True:
        file_spliting_seperator= '_'
        numerical_index = 3 # ex routes_sample_basin_0_top_bottom_matrix.csv = 0
        files = (scan_directory(MERGE_FILE_INPUT_PATH,'.csv'))
        # Skip the Merged_file if already in directory.  It will be deleted.
        for n, f in enumerate(files):
            if MERGED_FILE_NAME in os.path.basename(f):
                #del(files[n])
                sys.exit('Delete ', MERGED_FILE_NAME,
                         'from your input directory' )
        try:
            #sorted_files = sorted(files, key = lambda name:int(((os.path.basename(name)).split(file_spliting_seperator))[numerical_index]))
            sorted_files = sorted(files,key = lambda name:int((os.path.splitext(os.path.basename(name))[0].split(file_spliting_seperator))[numerical_index]))
            files = sorted_files
        except :
            # TODO Better sorting control MAKE A FUNCTION FOR THIS
            print ('Cannot sort files name by alpha-numerically',\
                   'Merged file will be created but will no be in alpha-numerical order')
        res_merge_files = (merge_files(files))
        
    print()  
    stop_time = time.time()
    total_time = (stop_time-START_TIME)/60
    print ('All done: ',total_time, 'minutes')
   
